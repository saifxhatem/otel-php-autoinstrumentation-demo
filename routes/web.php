<?php

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use OpenTelemetry\API\Trace\SpanKind;
use OpenTelemetry\SDK\Trace\TracerProvider;
use OpenTelemetry\SDK\Trace\TracerProviderFactory;
use OpenTelemetry\SDK\Trace\SpanProcessor\SimpleSpanProcessor;
use OpenTelemetry\SDK\Trace\SpanExporter\ConsoleSpanExporterFactory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    $response = Http::get('http://example.com');
    return $response->status();
});


Route::get('/timer', function () {
    sleep(5);
    return true;
});

Route::get('/phpinfo', function () {
    return phpinfo();
});


Route::get('/ping-demo-1', function () {
    $response = Http::get('http://nginx-1');
    return $response->status();
});

Route::get('/ping-demo-2', function () {
    $response = Http::get('http://nginx-2');
    return $response->status();
});


Route::get('/test', [TestController::class, 'test']);